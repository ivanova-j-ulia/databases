Exercise 1  
SELECT model, speed, hd  
FROM PC  
WHERE price < 500;  
  
Exercise 2  
SELECT DISTINCT maker  
FROM Product  
WHERE type = 'Printer';  
  
Exercise 3  
SELECT model, ram, screen  
FROM Laptop  
WHERE price > 1000;  
  
Exercise 4  
SELECT *  
FROM Printer  
WHERE color = 'y';  
  
Exercise 5  
SELECT model, speed, hd  
FROM PC  
WHERE (cd = '12x' OR cd = '24x') AND price < 600;  
  
Exercise 6  
SELECT DISTINCT Product.maker, speed  
FROM Product INNER JOIN Laptop  
ON Product.model = Laptop.model AND Laptop.hd >= 10;  
  
Exercise 7  
SELECT Product.model, price 
FROM Product INNER JOIN PC 
ON Product.model = PC.model 
WHERE Product.maker = 'B' 
UNION 
SELECT Product.model, price 
FROM Product INNER JOIN Laptop 
ON Product.model = Laptop.model 
WHERE Product.maker = 'B' 
UNION 
SELECT Product.model, price 
FROM Product INNER JOIN Printer 
ON Product.model = Printer.model 
WHERE Product.maker = 'B'; 

Exercise 8
SELECT DISTINCT maker
FROM Product
WHERE type = 'PC' AND 
maker NOT IN (SELECT maker 
FROM Product 
WHERE type = 'Laptop');

Exercise 9
SELECT DISTINCT maker
FROM Product INNER JOIN PC 
ON Product.model = PC.model
WHERE speed >=450;

Exercise 10
SELECT model, price
FROM Printer
WHERE price = (SELECT MAX(price)
FROM Printer);

Exercise 11
SELECT AVG(speed)
FROM PC;

Exercise 12
SELECT AVG(speed)
FROM Laptop
WHERE price > 1000;

Exercise 13
SELECT AVG(speed)
FROM PC
WHERE PC.model IN (
SELECT model
FROM Product
WHERE maker = 'A');

Exercise 14	
	Solution 1:
SELECT maker, type
FROM Product
WHERE type = 'PC' AND maker NOT IN (
SELECT maker
FROM Product
WHERE type != 'PC'
)
GROUP BY type, maker
HAVING COUNT(model) > 1

UNION

SELECT maker, type
FROM Product
WHERE type = 'Laptop' AND maker NOT IN (
SELECT maker
FROM Product
WHERE type != 'Laptop'
)
GROUP BY type, maker
HAVING COUNT(model) > 1

UNION

SELECT maker, type
FROM Product
WHERE type = 'Printer' AND maker NOT IN (
SELECT maker
FROM Product
WHERE type != 'Printer'
)
GROUP BY type, maker
HAVING COUNT(model) > 1

	Solution 2:
SELECT DISTINCT maker, type FROM Product 
WHERE maker IN (
	SELECT maker FROM Product 
	GROUP BY maker HAVING COUNT(DISTINCT type) = 1 AND COUNT(model) > 1
);

Exercise 15
SELECT hd
FROM PC
GROUP BY hd
HAVING COUNT(model) >=2;
