WITH recursive result AS (
 SELECT id, pid
 FROM test_table
 WHERE id = 3

 UNION

 SELECT test_table.id, test_table.pid
 FROM test_table JOIN result 
 ON test_table.id = result.pid
) SELECT * FROM result;

