--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.1
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: test_table; Type: TABLE; Schema: public; Owner: julia
--

CREATE TABLE test_table (
    id integer NOT NULL,
    pid integer
);


ALTER TABLE test_table OWNER TO julia;

--
-- Data for Name: test_table; Type: TABLE DATA; Schema: public; Owner: julia
--

COPY test_table (id, pid) FROM stdin;
1	2
2	3
3	4
4	5
5	6
7	6
9	6
6	10
11	7
\.


--
-- Name: test_table_pkey; Type: CONSTRAINT; Schema: public; Owner: julia
--

ALTER TABLE ONLY test_table
    ADD CONSTRAINT test_table_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

