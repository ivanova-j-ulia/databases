Ex 28
SELECT CAST(AVG(CAST(sum AS NUMERIC(6,2))) AS NUMERIC(6,2))FROM (
 SELECT CAST(COALESCE(SUM(B_VOL), 0) AS NUMERIC(6,2)) AS sum
 FROM utB RIGHT JOIN utQ
 ON utQ.Q_id = utB.B_Q_id
 GROUP BY Q_ID
) X;

Ex 29
SELECT ISNULL(i.point, o.point), ISNULL(i.date, o.date), i.inc, o.out FROM (
 (SELECT * FROM Income_o) i
 FULL JOIN
 (SELECT * FROM Outcome_o) o
 ON i.point = o.point AND i.date = o.date
);

Ex 30
SELECT point, date,
IIF(SUM(out) = 0, NULL, SUM(out)) out,
IIF(SUM(inc) = 0, NULL, SUM(inc)) inc
FROM (
  SELECT point, date, out, 0 AS inc FROM Outcome
  UNION ALL
  SELECT point, date, 0 AS out, inc FROM Income
) AS X
GROUP BY point, date;

Ex 31
SELECT class, country FROM CLasses
WHERE bore >= 16;

Ex 32
SELECT country, CAST(AVG((POWER(bore,3) /2)) AS NUMERIC(6,2)) AS mw 
FROM Classes INNER JOIN (
  SELECT class FROM Ships
  UNION ALL
  SELECT DISTINCT ship as class FROM Outcomes WHERE ship NOT IN (
    SELECT name FROM Ships
  )
) X
ON Classes.class = X. class
GROUP BY country;

Ex 33
SELECT ship FROM Outcomes
WHERE battle = 'North Atlantic' AND result = 'sunk';

Ex 34
SELECT name FROM
Ships Join Classes
ON Ships.class = Classes.class
WHERE displacement > 35000 AND type = 'bb' AND launched >= 1922;

Ex 35 
SELECT model, type FROM Product
WHERE NOT (model LIKE '%[^0-9]%' AND model LIKE '%[^a-zA-Z]%');

Ex 36
SELECT class FROM Classes
WHERE class IN (
 SELECT name FROM Ships
 UNION
 SELECT ship FROM Outcomes
);

Ex 37
SELECT class FROM 
Classes  JOIN (
 SELECT class AS x FROM Ships
 UNION ALL
 SELECT DISTINCT ship AS x FROM Outcomes WHERE ship NOT IN (
  SELECT DISTINCT name FROM Ships
  )
) R
ON class = R.x
GROUP BY class
HAVING COUNT(*) = 1;

Ex 38
SELECT country FROM Classes
WHERE type = 'bb'
INTERSECT
SELECT country FROM Classes
WHERE type = 'bc';

Ex 39
SELECT DISTINCT ship FROM (
 SELECT ship, date FROM
 Outcomes O JOIN Battles AS B
 ON B.name = O.battle
 WHERE result = 'damaged' AND EXISTS(
  SELECT ship, date FROM
  Outcomes O2 JOIN Battles
  ON Battles.name = O2.battle
  WHERE date > B.date AND O2.ship = O.ship
 )
) X;

Ex 40
SELECT Ships.class, name, country FROM
Classes JOIN Ships
ON Classes.class = Ships.class
WHERE numGuns >= 10;

Ex 41
WITH R AS (
 SELECT model, speed, ram, hd, cd, price FROM PC
 WHERE code IN (SELECT MAX(code) FROM PC)
)
SELECT 'model' AS char, CAST(model AS CHAR(10)) AS val FROM R
UNION
SELECT 'speed' AS char, CAST(speed AS CHAR(10)) AS val FROM R
UNION
SELECT 'ram' AS char, CAST(ram AS CHAR(10)) AS val FROM R
UNION
SELECT 'hd' AS char, CAST(hd AS CHAR(10)) AS val FROM R
UNION
SELECT 'cd' AS char, CAST(cd AS CHAR(10))AS val FROM R
UNION
SELECT 'price' AS char, CAST(price AS CHAR(10)) AS val FROM R;

Ex 42
SELECT ship, battle FROM Outcomes
WHERE result = 'sunk';

Ex 43
SELECT name FROM Battles
WHERE YEAR(date) NOT IN (
 SELECT launched FROM Ships
 WHERE launched IS NOT NULL
);

Ex 44
SELECT name FROM Ships 
WHERE name LIKE 'R%'
UNION
SELECT ship FROM Outcomes 
WHERE ship LIKE 'R%';

Ex 45
SELECT name FROM Ships 
WHERE name LIKE '% % %'
UNION
SELECT ship FROM Outcomes 
WHERE ship LIKE '% % %';

