CREATE INDEX index_by_debt ON "Connection" (debt);
CREATE INDEX index_by_user_passport ON "Connection" (user_passport);
CREATE INDEX index_by_passport ON "User" (passport);
