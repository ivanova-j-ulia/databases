WITH R AS (
 SELECT "User".name AS name, "Operator".name AS operator, debt FROM
 "Operator" JOIN "Tariff" ON "Operator".id = "Tariff".oper_id
 JOIN "Connection" ON "Connection".tariff_id = "Tariff".id
 JOIN "User" ON "User".passport = "Connection".user_passport
)
SELECT name FROM R 
WHERE operator = 'mts' AND debt >= CAST(100 AS money);
