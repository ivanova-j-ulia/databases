CREATE TABLE "Operator" (
	"name" varchar(15) NOT NULL,
	"id" integer PRIMARY KEY,
	"qty_numbers" integer NOT NULL,
	CHECK (qty_numbers > 0)
) WITH (
  OIDS=FALSE
);

CREATE TABLE "User" (
	"name" varchar(35) NOT NULL,
	"passport" integer PRIMARY KEY,
	"adress" varchar(35) NOT NULL
) WITH (
  OIDS=FALSE
);

CREATE TABLE "Tariff" (
	"id" integer PRIMARY KEY,
	"oper_id" integer NOT NULL,
	"name" varchar(20) NOT NULL,
	CONSTRAINT fk1
	FOREIGN KEY ("oper_id") REFERENCES "Operator"("id")
	ON DELETE CASCADE ON UPDATE CASCADE
) WITH (
  OIDS=FALSE
);

CREATE TABLE "Connection" (
	"id" integer NOT NULL PRIMARY KEY,
	"tariff_id" integer NOT NULL,
	"debt" money NOT NULL,
	"date" DATE NOT NULL,
	"user_passport" integer,
	FOREIGN KEY ("tariff_id") REFERENCES "Tariff"("id"),
	CONSTRAINT fk1 
	FOREIGN KEY ("user_passport") REFERENCES "User"("passport")
	ON DELETE CASCADE ON UPDATE CASCADE
) WITH (
  OIDS=FALSE
);


