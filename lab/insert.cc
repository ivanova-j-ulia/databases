#include <cstdio>
#include <pqxx/connection>
#include <pqxx/transaction>
#include <cstdlib>
#include <ctime>
#include <set>
//#include <string>

int main() {
	//connect to db
	pqxx::connection conn("dbname=julia user=julia");
	pqxx::work xact(conn, "RandomData");

	srand(time(NULL));
	long tariff_id = 0;
	long conn_id = 0;

	char query[256];
	for (int i = 0; i < 10; ++i) {
		long qty_numbers = 1000000L;
		sprintf(query, "INSERT INTO \"Operator\" (name, id, qty_numbers) VALUES ('%c', %d, %ld);", '0' + i, i, qty_numbers);
		xact.exec(query);
		int qty_tariffs = rand() % 8 + 3;
		for (int t = 0; t < qty_tariffs; ++t) {
			char name[10] = "noname";
			sprintf(query, "INSERT INTO \"Tariff\" (id, oper_id, name) VALUES (%ld, %d, '%s');", tariff_id, i, name);
			xact.exec(query);
			++tariff_id;
		}
	}

	for (long i = 0; i < 500000L; ++i) {
		char name[10] = "some name";
		char adress[10] = "adress";
		sprintf (query, "INSERT INTO \"User\" (name, passport, adress) VALUES ('%s', %ld, '%s');", name, i, adress);
		xact.exec(query);
		int qty_conn = rand() % 5 + 1;
		for (int j = 0; j < qty_conn; ++j) {
			long tariff = rand() % tariff_id;
			int debt = rand() % 5000;
			sprintf(query, "INSERT INTO \"Connection\" (id, user_passport, tariff_id, debt, date) "
				"VALUES (%ld, %ld, %ld, %d, date \'2014-01-01\')", conn_id, i, tariff, debt);
			xact.exec(query);
	     	++conn_id;
		}
	}

	xact.commit();


	return 0;
}