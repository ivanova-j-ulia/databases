WITH R AS (
 SELECT "Operator".name AS operator, "Tariff".name AS tariff, COUNT(tariff_id) AS cnt FROM
 "Operator"
 JOIN "Tariff" 
 ON "Operator".id = oper_id
 JOIN "Connection"
 ON "Tariff".id = tariff_id
 GROUP BY operator, tariff
)

SELECT operator, tariff FROM R
WHERE cnt IN (
 SELECT MAX(cnt) FROM R AS R2
 WHERE R.operator = operator
)
