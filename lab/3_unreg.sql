WITH R AS (
 SELECT "Operator".name AS operator, "Connection".id AS conn_id, qty_numbers FROM
 "Operator" JOIN "Tariff" ON "Operator".id = "Tariff".oper_id
 JOIN "Connection" ON "Tariff".id = "Connection".tariff_id
), Reg AS (
 SELECT operator, qty_numbers, COUNT(conn_id) AS cnt FROM R
 GROUP BY operator, qty_numbers
)
SELECT operator, qty_numbers - cnt AS unreg FROM Reg;
