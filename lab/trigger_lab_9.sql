CREATE FUNCTION conn_on_insert() RETURNS trigger AS $$
BEGIN
IF ( 
 SELECT COUNT("Connection".id) FROM 
 "Operator" JOIN "Tariff" ON "Operator".id = oper_id
 JOIN "Connection" ON "Tariff".id = tariff_id
 WHERE "Operator".id IN (
  SELECT "Operator".id FROM "Operator" JOIN "Tariff"
  ON "Operator".id = oper_id
  WHERE "Tariff".id = NEW.tariff_id
 )
 GROUP BY "Operator".id
) >= (
 SELECT DISTINCT qty_numbers FROM
 "Operator" JOIN "Tariff" ON "Operator".id = oper_id
 JOIN "Connection" ON "Tariff".id = tariff_id
 WHERE "Operator".id IN (
  SELECT "Operator".id FROM "Operator" JOIN "Tariff"
  ON "Operator".id = oper_id
  WHERE "Tariff".id = NEW.tariff_id
 )
)
 THEN RETURN NULL;
ELSE
 RETURN NEW;
END IF;
END;
$$ LANGUAGE  plpgsql;

CREATE TRIGGER trigger_conn_on_insert
BEFORE INSERT ON "Connection" FOR EACH ROW
EXECUTE PROCEDURE conn_on_insert();

