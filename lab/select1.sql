WITH R AS (
 SELECT "Operator".name AS operator, "Connection".id AS conn_id FROM
 "Operator" JOIN "Tariff" ON "Operator".id = "Tariff".oper_id
 JOIN "Connection" ON "Tariff".id = "Connection".tariff_id
), Reg AS (
 SELECT operator, COUNT(conn_id) AS cnt FROM R
 GROUP BY operator
)
SELECT row_number() over(ORDER BY cnt DESC) AS n, operator, cnt FROM Reg;
