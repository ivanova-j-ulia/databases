INSERT INTO "Operator" (name, id, qty_numbers)
VALUES ('mts', 985, 5), --4 unreg
('megafon', 925, 50), --2 unreg
('beeline', 903, 50); -- 0 unreg

-----

INSERT INTO "Tariff" (id, oper_id, name) VALUES
  (1, 985, 'smart'),
  (2, 985, 'smartmini'),
  (3, 985, 'super'),
  (4, 925, 'VIP'),
  (5, 925, 'S'),
  (6, 925, 'L'),
  (7, 903, 'vse'),
  (8, 903, 'vse family');

--------

INSERT INTO "User" (name, passport, adress)
VALUES ('Ilushin Petr Olegovich', 010203, 'Lenina str, 9'),
('Michaylova Anna Leonidovna', 123456, 'Elevatornaya str, 32'),
('Nechay Viktoriya Andreevna', 424242, 'Yasnaya str, 2'),
('Ivaschenko Aleksandr Vitalievich', 141414, 'Dekabristov str, 56'),
('Yakimchuk Yarina Vladimiromna', 321321, 'Kirova str, 21'),
('Melnik Dmitriy Alexeevich', 098098, 'Sevastopolskaya str, 78'),
('Mogunova Yulia Sergeevna', 678876, 'Franko str, 4'),
('Ivanova Yulia Alexandrovna', 000000, 'Bolshoy str, 16'),
('Volodin Sergey Valerievich', 2648795, 'Proletarsk7str, 55'),
('Ivaschenko Nadezda Vyacheslavovna', 135462, 'Kievskaya str, 278'),
('Andreeva Natalya Sergeevna', 741258, 'Bolshevitskaya str, 39'),
('Golovko Anastasiya Vitalievna', 963214, 'Franko str, 8');

------

INSERT INTO "Connection" (id, user_passport, tariff_id, debt, date)
VALUES
(1, 010203, 1, 0, date '2014-05-23'),
(2, 123456, 5, 8, date '2015-11-09'),
(3, NULL, 2, 10, date '2013-02-28'),
(4, 141414, 6, 50, date '2012-06-24'),
(5, 321321, 3, 300, date '2015-09-09'),
(6, NULL, 4, 144, date '2015-09-19'),
(7, 678876, 1, 95, date '2011-06-16'),
(8, 000000, 5, 64, date '2013-06-01'),
(9, 2648795, 1, 42, date '2012-12-12'),
(10, NULL, 6, 3, date '2010-12-12'),
(11, 741258, 2, 123, date '2013-12-31'),
(12, 963214, 5, 5, date '2014-04-11'),
(13, NULL, 3, 749, date '2015-02-14'),
(14, 098098, 8, 65, date '2015-03-13'),
(15, NULL, 2, 47, date '2015-11-02'),
(16, 424242, 7, 654, date '2013-06-07'),
(17, NULL, 1, 90, date '2012-04-04'),
(18, 135462, 7, 0, date '2015-09-30'),
(19, NULL, 1, 15, date '2016-01-01'),
(20, 010203, 3, 673, date '2015-09-23'),
(21, 010203, 7, 158, date '2015-04-01'),
(22, 741258, 1, 94, date '2011-03-11');
