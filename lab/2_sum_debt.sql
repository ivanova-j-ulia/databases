SELECT "Operator".name AS operator, "Tariff".name AS tariff, SUM(debt) AS debt FROM
"Operator" JOIN "Tariff"
ON "Operator".id = oper_id
JOIN "Connection"
ON "Tariff".id = tariff_id
GROUP BY operator, tariff;
