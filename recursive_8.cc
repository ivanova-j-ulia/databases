#include <stdio.h>
#include <sqlite3.h>

int callback(void *data, int row_size, char **row, char **column_names) {
  printf("%s\n", row[0]);

  return 0;
}

int main() {

  printf("Enter name of your database:\n");
  char db_name[256];
  scanf("%s", db_name);

  printf("Enter ID of element you want to start from:\n");
  int id;
  scanf("%d", &id);

  printf("Enter depth of search:\n");
  int depth;
  scanf("%d", &depth);

  sqlite3 *db;
  if (sqlite3_open(db_name, &db)) 
    printf("Failed to open database.\n");

  char query[1000];
  sprintf(query, "WITH RECURSIVE result AS ("
                 "SELECT id, pid, 0 AS step"
                 "FROM test_table"
                 "WHERE id = i"

                 "UNION"

                 "SELECT test_table.id, test_table.pid, step + 1 AS step"
                 "FROM test_table JOIN result "
                 "ON test_table.id = result.pid"
                 "WHERE step < k"
                 ")"
                 "SELECT DISTINCT id FROM result"
                 "WHERE step = k;"
                , id, depth);

  char *error_message;
  if(sqlite3_exec(db, query, callback, NULL, &error_message) != SQLITE_OK)
    printf("Failed to execute query.\n %s", error_message);

  sqlite3_close(db);

  return 0;
}
