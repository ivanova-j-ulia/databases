Exercise 16
SELECT DISTINCT A.model AS model, B.model AS model, A.speed, A.ram
FROM PC AS A, PC AS B
WHERE A.speed = B.speed AND A.ram = B.ram AND A.model > B.model;

Exercise 17
SELECT DISTINCT type, Product.model, speed
FROM Laptop, Product
WHERE Laptop.model = Product.model AND Laptop.speed < ALL (
SELECT speed
FROM PC
);

Exercise 18
SELECT DISTINCT maker, price
FROM Printer JOIN Product ON Printer.model = Product.model
WHERE color = 'y' AND price = ( 
SELECT MIN(price) FROM Printer 
WHERE color = 'y'
);

Exercise 19
SELECT DISTINCT maker, AVG(screen) AS Avg_screen
FROM Product INNER JOIN Laptop ON Product.model = Laptop.model
GROUP BY maker;

Exercise 20
SELECT DISTINCT maker, COUNT(DISTINCT model) AS Count_model
FROM Product
WHERE type = 'PC'
GROUP BY maker
HAVING COUNT(DISTINCT model) >= 3;

Exercise 21
SELECT maker, MAX(price) AS Max_price
FROM Product INNER JOIN PC ON Product.model = PC.model
GROUP BY maker;

Exercise 22
SELECT speed, AVG(price) AS Avg_price
FROM PC
WHERE speed > 600
GROUP BY speed;

Exercise 23
SELECT maker
FROM PC INNER JOIN Product ON PC.model = Product.model
WHERE speed >= 750

INTERSECT 

SELECT maker
FROM Laptop INNER JOIN Product ON Laptop.model = Product.model
WHERE speed >= 750;

Exercise 24
WITH tmp AS(
 SELECT price, model
 FROM PC
 UNION 
 SELECT price, model
 FROM Laptop
 UNION
 SELECT price, model
 FROM Printer
)
SELECT DISTINCT model
FROM tmp
WHERE price = (
SELECT MAX(price)
FROM tmp
);

Exercise 25
SELECT DISTINCT maker
FROM Product JOIN PC  ON Product.model = PC.model
WHERE ram = (SELECT MIN(ram) FROM PC) AND
speed = (SELECT MAX(speed) 
FROM PC
WHERE ram = (SELECT MIN(ram) FROM PC)
)AND 
maker IN(SELECT maker 
FROM Product
WHERE type = 'Printer'
);

Exercise 26
SELECT AVG(price)
FROM (
SELECT price 
FROM Product JOIN PC ON Product.model = PC.model
WHERE maker = 'A'

UNION ALL

SELECT price 
FROM Product JOIN Laptop ON Product.model = Laptop.model
WHERE maker = 'A'
) AS tmp
;

Exercise 27
SELECT maker, AVG(hd)
FROM Product JOIN PC ON Product.model = PC.model
WHERE maker IN(SELECT maker FROM Product
WHERE type = 'printer')
GROUP BY maker;


