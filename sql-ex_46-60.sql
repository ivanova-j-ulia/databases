Ex 46
WITH R AS (
 SELECT ship FROM Outcomes
 WHERE battle = 'Guadalcanal'
)
SELECT ship, displacement, numGuns FROM
R LEFT JOIN Ships
ON R.ship = Ships.name
LEFT JOIN Classes
ON Classes.class = R.ship OR Classes.class = Ships.class;

Ex 47
WITH R AS (
 SELECT maker, COUNT(model) cnt FROM Product 
 GROUP BY maker
) 
SELECT ROW_NUMBER() OVER(ORDER BY cnt DESC, Product.maker, model) no,
Product.maker, model
FROM Product JOIN R ON Product.maker = R.maker;

Ex 48
SELECT Classes.class FROM
SELECT DISTINCT Classes.class FROM
Classes LEFT JOIN Ships
ON Classes.class = Ships.class
JOIN Outcomes
ON Classes.class = ship OR Ships.name = ship
WHERE result = 'sunk';

Ex 49
SELECT name FROM 
Classes JOIN Ships
ON Classes.class = Ships.class
WHERE bore = 16
UNION
SELECT ship name FROM
Classes JOIN Outcomes
ON Classes.class = Outcomes.ship
WHERE bore = 16;

Ex 50
SELECT DISTINCT battle FROM
Ships JOIN Outcomes
ON name = ship
WHERE class = 'Kongo';

Ex 51
WITH R AS (
 SELECT name, displacement, numGuns FROM
 Ships JOIN Classes 
 ON Ships.class = Classes.class
 UNION
 SELECT ship AS name, displacement, numGuns FROM
 Outcomes JOIN Classes 
 ON ship = class
) 
SELECT name FROM R 
WHERE numGuns >= ALL (
 SELECT numGuns FROM R AS R2 
 WHERE R.displacement= R2.displacement
);

Ex 52
WITH R AS(
 SELECT name, COALESCE(type, 'bb') t, COALESCE(country, 'Japan') c,
 COALESCE(numGuns, 9) n, COALESCE(bore, 0) b, COALESCE(displacement, 0) d
 FROM
 Ships LEFT JOIN Classes
 ON Ships.class = Classes.class
)
SELECT name FROM R 
WHERE t = 'bb' AND c = 'Japan' AND n >= 9 AND b < 19
AND d <= 65000;

Ex 53
SELECT CAST(AVG(CAST(numGuns AS NUMERIC(6,2))) AS NUMERIC(6,2)) FROM Classes
WHERE type = 'bb';

Ex 54
WITH R AS (
 SELECT DISTINCT numGuns, name FROM
 Classes JOIN Ships
 ON Classes.class = Ships.class
 WHERE type = 'bb'
 UNION
 SELECT DISTINCT numGuns, class AS name FROM
 Classes
 WHERE class IN (
  SELECT ship FROM Outcomes
 )
 AND type = 'bb'
)
SELECT CAST(AVG(CAST(numGuns AS NUMERIC(6,2))) AS NUMERIC(6,2)) FROM R;

Ex 55
SELECT Classes.class, MIN(launched) FROM 
Classes LEFT JOIN Ships 
ON Classes.class = Ships.class
GROUP BY Classes.class;

Ex 56
WITH R AS (
 SELECT Classes.class, COUNT(*) cnt FROM
 Classes JOIN Ships
 ON Classes.class = Ships.class
 JOIN Outcomes ON ship = name
 WHERE result = 'sunk'
 GROUP BY Classes.class
 UNION ALL
 SELECT class, 1 AS cnt FROM
 Classes JOIN Outcomes
 ON Classes.class = Outcomes.ship
 WHERE result = 'sunk' AND class NOT IN (
  SELECT name FROM Ships
 )
 UNION ALL
 SELECT class, 0 AS cnt FROM Classes
)
SELECT class, SUM(cnt) FROM R
GROUP BY class;

Ex 57
WITH R AS(
 SELECT C.class, COUNT(*) cnt FROM 
 Classes C JOIN Ships S 
 ON C.class = S.class
 JOIN Outcomes 
 ON ship = name AND result = 'sunk'
 GROUP BY C.class
 UNION ALL
 SELECT class, 1 cnt FROM 
 Classes C  JOIN Outcomes O 
 ON C.class = O.ship
 WHERE result = 'sunk' AND class NOT IN (SELECT name FROM ships)
 UNION ALL
 SELECT class, 0 cnt FROM Classes
) 
SELECT class, sum(cnt) FROM R
WHERE class IN (
 SELECT Classes.class FROM 
 Classes JOIN (
  SELECT name, class FROM Ships
  UNION
  SELECT ship name, ship class FROM Outcomes
 ) X 
 ON Classes.class = X.class 
 GROUP BY Classes.class HAVING COUNT(name) >= 3
) 
GROUP BY class HAVING sum(cnt) > 0;

Ex 58
SELECT M.maker, T.type, 
CAST(100 * CAST(COUNT(model) AS real) / cnt AS DECIMAL(10,2))FROM (
 SELECT DISTINCT maker FROM Product
) M
CROSS JOIN (
 SELECT DISTINCT type FROM Product
) T
LEFT JOIN Product P 
ON M.maker = P.maker AND T.type = P.type
JOIN (
 SELECT maker, CAST(COUNT(*) AS real) cnt FROM Product 
 GROUP BY maker
) MC 
ON M.maker = MC.maker 
GROUP BY M.maker, T.type, cnt;

Ex 59
SELECT point, SUM(inc) AS balance FROM (
 SELECT point, inc FROM Income_o
 UNION ALL
 SELECT point, -out as inc FROM Outcome_o
) AS payments 
GROUP BY point;

Ex 60
SELECT point, SUM(inc) balance FROM (
 SELECT point, inc  FROM Income_o 
 WHERE date < '2001-04-15'
 UNION ALL
 SELECT point, -out as inc FROM Outcome_o 
 WHERE date < '2001-04-15'
) X 
GROUP BY point;
